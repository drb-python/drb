===================
Data Request Broker
===================
---------------------------------
Python Data Request Broker (DRB)
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb/month
    :target: https://pepy.tech/project/drb
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb.svg
    :target: https://pypi.org/project/drb/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb.svg
    :target: https://pypi.org/project/drb/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb.svg
    :target: https://pypi.org/project/drb/
    :alt: Python Version Support Badge

-------------------

Getting started
===============
.. toctree::
   :maxdepth: 2

   user/what_is_drb
   user/concepts
   user/install
   user/getting_started

Developing with DRB
===================
.. toctree::
   :maxdepth: 2

   dev/api
   dev/developing

Others
======
.. toctree::
   :maxdepth: 1

   other/contact