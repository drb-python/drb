.. _api:

API Reference
=============

DrbNode
-------
.. autoclass:: drb.core.node.DrbNode
  :members:
  :private-members: _event_manager
  :special-members: __len__, __getitem__, __setitmen__, __delitem__, __contains__, __hash__, __imatmul__, __matmul__

Events
______
+-----------------------------------------------+----------------------------------------------------+
| Event                                         | Short Description                                  |
+===============================================+====================================================+
| :func:`name-changed <name_changed>`           | Emitted when the node name is changed              |
+-----------------------------------------------+----------------------------------------------------+
| :func:`namespace-changed <namespace_changed>` | Emitted when the node namespace is changed         |
+-----------------------------------------------+----------------------------------------------------+
| :func:`value-changed <value_changed>`         | Emitted when the node value is changed             |
+-----------------------------------------------+----------------------------------------------------+
| :func:`child-added <child_added>`             | Emitted when the a new child is added to the node  |
+-----------------------------------------------+----------------------------------------------------+
| :func:`child-removed <child_removed>`         | Emitted when a child is removed from the node      |
+-----------------------------------------------+----------------------------------------------------+
| :func:`child-changed <child_changed>`         | Emitted when a child is replace by another node    |
+-----------------------------------------------+----------------------------------------------------+
| :func:`attribute-added <attribute_added>`     | Emitted when an attribute is added to the node     |
+-----------------------------------------------+----------------------------------------------------+
| :func:`attribute-removed <attribute_removed>` | Emitted when an attribute is removed from the node |
+-----------------------------------------------+----------------------------------------------------+
| :func:`attribute-changed <attribute_changed>` | Emitted when an attribute value is changed         |
+-----------------------------------------------+----------------------------------------------------+

.. py:function:: attribute_added(node: DrbNode, name: str, namespace: Optional[str], value: Optional[Any])

    Emitted when a new attribute is added to the node.

    :param DrbNode node: the node which sent the event
    :param str name: added attribute name
    :param str namespace: added attribute namespace
    :param Any value: added attribute value


.. py:function:: attribute_removed(node: DrbNode, name: Any, namespace: Optional[str], value: Optional[Any])

    Emitted when an attribute is removed from the node.

    :param DrbNode node: the node which sent the event
    :param str name: removed attribute name
    :param str namespace: removed attribute namespace
    :param Any value: removed attribute value


.. py:function:: attribute_changed(node: DrbNode, name: Any, namespace: str, old: Any, new: Any)

    Emitted when an associated attribute value associated to the node is changed.

    :param DrbNode node: the node which sent the event
    :param str name: name of modified attribute
    :param str namespace: namespace URI of modified attribute
    :param Any old: old value of modified attribute
    :param Any new: new value of modified attribute


.. py:function:: child_added(node: DrbNode, name: str, namespace: Optional[str], value: Optional[Any])

    Emitted when a new child is added to the node.

    :param DrbNode node: the node which sent the event
    :param int index: index of added child node
    :param DrbNode child: added child node


.. py:function:: child_removed(node: DrbNode, index: int, child: DrbNode)

    Emitted when a child is removed from the node.

    :param DrbNode node: the node which sent the event
    :param int index: index of removed child node
    :param DrbNode child: removed child node


.. py:function:: child_changed(node: DrbNode, index: int, old: DrbNode, new: DrbNode)

    Emitted when a child is replace by another node.

    :param DrbNode node: the node which sent the event
    :param int index: index of replaced child node
    :param DrbNode old: old child node
    :param DrbNode new: new child node


.. py:function:: name_changed(node: DrbNode, old: str, new: str)

    Event emitted when the name of the node is changed.

    :param DrbNode node: the node which sent the event
    :param str old: old node name
    :param str new: new node name


.. py:function:: namespace_changed(node: DrbNode, old: str, new: str)

    Event emitted when the node namespace is changed.

    :param DrbNode node: the node which sent the event
    :param str old: old node namespace
    :param str new: new node namespace


.. py:function:: value_changed(node: DrbNode, old: Any, new: Any)

    Emitted when the node value is changed.

    :param DrbNode node: the node which sent the event
    :param str old: old node value
    :param str new: new node value

DrbTopic
---------
.. autoclass:: drb.topics.topic.DrbTopic
   :members:

DrbFactory
----------
.. autoclass:: drb.core.factory.DrbFactory
   :members:
   :private-members: _create

Signature
---------
.. autoclass:: drb.core.signature.Signature
   :members:
