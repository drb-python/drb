.. _developing:
.. _Python external plugin mechanism:

Extend DRB
==========
**DRB** registers external plugins using `Python external plugin mechanism`_.
Following section describe how to develop a supported **DRB** plugin.

Signature
---------
A external signature plugin must define an entry point in the group
``drb.signature`` having:

- name: signature name to be used within others topic description
- value: classpath of class extended the ``Signature`` interface.

.. code-block:: python

    from setuptools import setup

    setup(
        # others setup arguments
        entry_points={
            'drb.signature': 'foobar = drb.signature.foobar:FoobarSignature'
        }
    )

Driver
------
A external driver plugin must define an entry point in the group ``drb.driver``
having:

- name: driver name identifier, this identifier will be used in topic
  description the ``factory`` section.
- value: classpath of the factory able to generate the specific node.

.. code-block:: python

    from
    setup(
        # others setup arguments
        entry_points={
            'drb.driver': '<name>: drb.driver.module:MyFactory'
        }
    )

Topic
-----
A topic is describe by a *YAML* file named ``cortex.yaml``. A external to topic
plugin must define an entry point in the group ``drb.topic`` having as value
the Python package containing the ``cortex.yaml`` file describing the topic.

.. code-block:: python

    from setuptools import setup

    setup(
        # others setup arguments
        entry_points={
            'drb.topic': '<name> = drb.topic.xml'
        }
    )

Topic description
+++++++++++++++++
.. table::
   :widths: grid
   :align: center

   +--------------+----------+------------------+------------------------------------+
   | Field        | Required | Type             | Description                        |
   +==============+==========+==================+====================================+
   | id           | required | string           | A hexadecimal UUID representation  |
   |              |          |                  | following RCF 4122                 |
   +--------------+----------+------------------+------------------------------------+
   | subClassOf   | optional | string           | A hexadecimal UUID representation  |
   |              |          |                  | following RCF 4122                 |
   +--------------+----------+------------------+------------------------------------+
   | label        | required | string           | Item class label                   |
   +--------------+----------+------------------+------------------------------------+
   | description  | optional | string           | Item class description             |
   +--------------+----------+------------------+------------------------------------+
   | category     | required | string           | Item class category. Accepted      |
   |              |          |                  | values: SECURITY, PROTOCOL,        |
   |              |          |                  | CONTAINER or FORMATTING.           |
   +--------------+----------+------------------+------------------------------------+
   | factory      | optional | string           | Allowing to define a specific view |
   |              |          |                  | for a node of this item class      |
   +--------------+----------+------------------+------------------------------------+
   | forced       | optional | boolean          | Force the factory to create the    |
   |              |          |                  | node before checking the signature |
   +--------------+----------+------------------+------------------------------------+
   | signatures   | optional | array<Signature> | Describe recognition node mechanism|
   |              |          |                  | Each signature are independent and |
   |              |          |                  | work like a logical `AND`          |
   +--------------+----------+------------------+------------------------------------+

Signature description
+++++++++++++++++++++
Signatures is an array of ``signature`` elements. A signature defines an
identification mechanism of a node. A node matching one of these signatures
will be identified as a node of this class.

Each signature defines a criterion that a node must match. Aggregation of these
criterion represents the complete signature.

Following sub-section defines signatures provided by the **DRB** core.

NameSignature
*************
This signature allowing to check if the name of a node match a specific
regex.

example:

.. code-block:: yaml

   signature:
      name: .+\.(?i)zip

NamespaceSignature
******************
This signature allowing to check if the namespace of a node.

example:

.. code-block:: yaml

   signature:
      namespace: https://gael-systems.com/drb-python#

PathSignature
******************
This signature allowing to check if the path of a node match a specific
regex.

example:

.. code-block:: yaml

   signature:
      path: ^http(s)?://.+

AttributesSignature
*******************
This signature allowing to check if a node has specific attributes. Can also
check namespace and value of an attribute. ``attributes`` is an array of
attribute element describe below:

.. table:: attribute description
   :widths: grid
   :align: center

   +-----------+----------+----------+-----------------------------+
   | Field     | Required | Type     | Description                 |
   +===========+==========+==========+=============================+
   | name      | required | string   | attribute name              |
   +-----------+----------+----------+-----------------------------+
   | namespace | optional | string   | attribute namespace         |
   +-----------+----------+----------+-----------------------------+
   | value     | optional | boolean, | attribute value             |
   |           |          | number,  |                             |
   |           |          | string,  |                             |
   |           |          | null     |                             |
   +-----------+----------+----------+-----------------------------+

example:

.. code-block:: yaml

   signature:
      attributes:
         # check node has an attribute named odata-version
         - name: odata-version
         # check node has an attribute named Content-Type and check also its value
         - name: Content-Type
           value: application/json

ChildrenSignature
*****************
Allowing to check if a node has a child whose its name match a specific regex.
Can also check child namespace. ``children`` is an array of child element
describe below:

.. table:: attributes description
   :widths: grid
   :align: center

   +----------------+----------+----------+--------------------------------+
   | Field          | Required | Type     | Description                    |
   +================+==========+==========+================================+
   | name           | required | string   | child name regex               |
   +----------------+----------+----------+--------------------------------+
   | namespace      | optional | string   | child namespace                |
   +----------------+----------+----------+--------------------------------+
   | namespaceAware | optional | boolean  | active check node namespace    |
   |                |          |          | if not defined (default false) |
   +----------------+----------+----------+--------------------------------+

example:

.. code-block:: yaml

   signature:
      children:
         - name: (?i)manifest.safe
           namespaceAware: yes

PythonSignature
****************
Allowing to implement its own signature via a Python script. This script
**must** be compatible Python 3.8+ and **must** return a boolean value,
otherwise the signature element will never match any nodes. ``node`` is a
special variable injected in the script context to access to the current
node.

.. code-block:: yaml

    signatures:
        - python: |
            return len(node / 'child_name') > 3

Example
+++++++
.. code-block:: yaml

    id: a11b4fea-7d2a-11ec-90d6-0242ac120003
    label: SAFE Manifest
    description: |
        A long description
    factory: xml
    signatures:
        - name: manifest.safe

