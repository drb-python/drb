.. _contact:
.. _GitLab issue tracker: https://gitlab.com/drb-python/drb/-/issues

Contact
=======
Feel free to contact us for any question about **DRB** at drb-python@gael.fr.

If any troubleshoot happened using **DRB** please use the
`GitLab issue tracker`_ to submit a bug or a request feature.