.. _concepts:
.. _keyring: https://github.com/jaraco/keyring
.. _Bitwarden: https://bitwarden.com

Concept
=======

Node
----
**DRB** use a unique data model to represent any data call ``node``. A node is
characterized by its name, namespace and its value, a node may has a parent and
children which are also a ``node`` structure. This node hierarchy is used to
represent the datasets logical organisation.
Specific properties may also be attached to a node, one of theses properties
is called ``attribute``.

Topic
-----
``topic`` is defined to identify a node within its logical hierarchy. Thanks
to its :ref:`Signature` it carries out simple nodes recognitions
(*XML*, *SAFE*, *JSON*,…) as well as complex data structure
(*Sentinel-2 L1C*, *Landsat-8*, …).

Signature
_________
Signature represents one or more rules to recognize a topic.

Driver
------
A driver is the implementation of the node to dig into their logical hierarchy.
They are able to implement client to service or protocols (*HTTP*, *FTP*,
*WebDAV*, *OData*, *AWS3*, *Swift*, …), containers (*Tar*, *Zip*, *Zarr*) as
well as formats (*XML*, *NetCDF*, *TIFF*, …). They are referenced by
:ref:`Topic`. Known drivers are currently released here.

Resolver
--------
The *resolver* in **DRB** is a component able to retrieve the most adapted
topic for a node or an URL and generate the associated node to browse the
content.

.. literalinclude:: examples/sample_resolver.py
    :language: python

Addon
-----
An addon is an extension of a topic. Addon are usually used to attach
additional properties to a topic such as
`metadata extraction <https://gitlab.com/drb-python/metadata/add-ons>`_,
image extraction and rendering…

Keyring
-------
Some data may be protected. DRB can use keyring_ to retrieve credentials or
authentication in order to access to the targeted data.

DRB provide a keyring able to access to a Bitwarden keyring, others keyring
may be used (see more details here: keyring_)

.. literalinclude:: examples/sample_keyring.py
    :language: python
