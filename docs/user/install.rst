.. _install:


Installation
============

Core
----
**DRB** requires *Python 3.8+*

.. code-block:: shell

    pip install drb

Driver
------
Generic command to install a driver:

.. code-block:: shell

    pip install drb-driver-xxx

Topic
-----
Generic command to install a topic

.. code-block:: shell

    pip install drb-topic-xxx

Signature
---------
Generic command to install a signature

.. code-block:: shell

    pip install drb-signature-xxx
