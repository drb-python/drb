from drb.topics import resolver
# to  retrieve the most adapted topic and generate the associated node
topic, node = resolver.resolve('path/to/my/resource')

# to generate only the most adapted node
node = resolver.create('path/to/my/resource')

# retrieve a specific node in the resource
node = resolver.create('path/to/my/resource/child/sub-child')
