from drb.keyring.bitwarden import BitwardenKeyring
import keyring

# initialize keyring
# connecting to the default Bitwarden vault instance
keyring.set_keyring(BitwardenKeyring('<login>', '<master_password>'))
# connecting to a private Bitwarden vault instance
keyring.set_keyring(BitwardenKeyring(
    '<login>', '<master_password>', vault_url='<my_vault_url>'))

# retrieve credential
credential = keyring.get_credential('<service_url>', '<username>')

if credential is not None:
    # credential properties
    print(f'{credential.username} -- {credential.password}')

# check existence of a specific field
if 'api-key' in credential:
    # access to specific a field or None if is not found
    print(credential.get('api-key'))
    # access to specific a field or raise KeyError if the key is not found
    print(credential['api-key'])
