from drb.topics import resolver

node = resolver.create('foobar.xml')

# retrieve first child named foo
child = node['foo']

# retrieve all children named foo
children = node['foo', :]

# iterate all children of a node
for child in node:
    print(child.name)
