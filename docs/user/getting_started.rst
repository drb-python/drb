.. _getting_started:
.. _virtual environment: https://docs.python.org/3/tutorial/venv.html

Getting started
===============

Preparing environment
---------------------
It is recommended to use a `virtual environment`_ to properly manage a working
environment. Use the following command to generate and activate it:

.. code-block:: shell

    $> python3 -m venv venv
    $> . venv/bin/activate

Once the virtual environment successfully activated

.. code-block:: shell

    (venv) $> pip install drb

Examples
--------
In the following example, the resolver is used to identify the most adapted
driver to enter in the XML content and build its logical represent.

.. literalinclude:: examples/sample_example.py
    :language: python
