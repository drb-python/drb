.. _what_is_drb:
.. _DOM: https://dom.spec.whatwg.org/
.. _European Space Agency: https://esa.int/
.. _XQuery: https://www.w3.org/TR/xquery/
.. _GAEL Systems: https://www.gael-systems.com/
.. _W3C: https://www.w3.org/

What is DRB ?
=============

Overview
--------
The **Data Request Broker** (**DRB**) is a library for reading, writing and
processing heterogeneous data. **DRB** defines an abstraction layer providing an
unique data model to represent any data, that makes the handling of supported
data formats much easier.

Initially developed  and well-tried for accessing satellite imagery for
`European Space Agency`_ data. **DRB** is the result of twenty years of
expertise and know-how in data access programming and provides a complete set
of tools for solving simple and complex data access issues with minimum of
engineering efforts.

Background
----------
Product format is a major key point for the successful use of data in many
domains. In space domain, processing, archiving, distribution, end-users habits
and existing tools introduce different needs and constraints that must be taken
into account when a format is defined. This results exponential number of data
formats are created to respond to the operational needs.

DRB was born from this ascertainment: Trying to define a simple and extensible
data model able to simplify data access without reinvent the wheel at each new
format, reducing the coding skills required to implement new format and then,
reduce these recurrent costs.

Originated from `GAEL Systems`_ development team since 1994, primary called Fmt
(*C* implementation), improved with *C++ LiveLink* library, then the major
improvement with *Java* version born in 1999. DRB fell into the public domain
with its Open Source licence in 2006. It is still maintained by `Gael Systems`_
and partially financed by `European Space Agency`_, and internal efforts of the
company.

The *Python* version of **DRB** is the new major improvement of this library,
with the objective to improve the **DRB** accessibility, capabilities and
performances with *Python* interpreted language, its libraries database and
its great performances.

**DRB** comes with its query language. Historically the *Java* and *Python*
implements XQuery_ language defined by `W3C`_ to browse inside nodes structures
and retrieve data contents. The *Python* implementation also supports querying
nodes using Python language.
