from drb.core import DrbNode, DrbFactory
from drb.nodes.logical_node import WrappedNode


class ZipNode(WrappedNode):
    def __init__(self, node: DrbNode):
        super().__init__(node)


class ZipFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return ZipNode(node)
