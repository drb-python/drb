import uuid
from typing import Any, Optional, List, Dict, Tuple

from drb.core import DrbNode, DrbFactory, ParsedPath
from drb.exceptions.core import DrbException
from drb.nodes.abstract_node import AbstractNode


class MockUrlNode(AbstractNode):
    def __init__(self, source):
        super(MockUrlNode, self).__init__()
        if isinstance(source, DrbNode):
            self._path = source.path
        else:
            raise DrbException

    @property
    def name(self) -> str:
        return self._path.filename

    @property
    def namespace_uri(self) -> Optional[str]:
        return None

    @property
    def value(self) -> Optional[Any]:
        return None

    @property
    def attributes(self) -> Dict[Tuple[str, str], Any]:
        return {}

    @property
    def parent(self) -> Optional[DrbNode]:
        return None

    @property
    def path(self) -> ParsedPath:
        return self._path

    @property
    def children(self) -> List[DrbNode]:
        return []

    def get_attribute(self, name: str, namespace_uri: str = None) -> Any:
        raise DrbException

    def has_child(self, name: str = None, namespace: str = None) -> bool:
        return False


class MockUrlFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return MockUrlNode(node)
