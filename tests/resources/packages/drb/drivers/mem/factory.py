from drb.core import DrbNode, DrbFactory
from .node import DrbMemNode


class DrbMemFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return DrbMemNode(f'Mem_{node.name}')
