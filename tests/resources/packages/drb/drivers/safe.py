from drb.core import DrbNode, DrbFactory


class SafeFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return node
