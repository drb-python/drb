from typing import Optional, Any, List, Dict, Tuple

from drb.core import DrbNode, DrbFactory, ParsedPath
from drb.nodes.abstract_node import AbstractNode


class DrbFoobarNode(AbstractNode):
    def __init__(self, name):
        super().__init__()
        self._name = f'Foobar_{name}'

    @property
    def name(self) -> str:
        return self._name

    @property
    def namespace_uri(self) -> Optional[str]:
        return None

    @property
    def value(self) -> Optional[Any]:
        return None

    @property
    def attributes(self) -> Dict[Tuple[str, str], Any]:
        return {}

    def get_attribute(self, name: str, namespace_uri: str = None) -> Any:
        pass

    @property
    def parent(self) -> Optional[DrbNode]:
        return None

    @property
    def children(self) -> List[DrbNode]:
        return []

    @property
    def path(self) -> Optional[ParsedPath]:
        return None

    def has_child(self, name: str = None, namespace: str = None) -> bool:
        return False


class DrbFoobarFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return DrbFoobarNode(node.path.filename)
