from drb.core import DrbNode, DrbFactory
from drb.nodes.logical_node import WrappedNode


class MockDataNode(WrappedNode):
    def __init__(self, node: DrbNode):
        super().__init__(node)

    @property
    def name(self) -> str:
        return self._wrapped.name[:-5]


class MockDataFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return MockDataNode(node)
