from typing import Any, Optional, List, Dict, Tuple

from drb.core import DrbNode, DrbFactory, ParsedPath
from drb.exceptions.core import DrbException
from drb.nodes.abstract_node import AbstractNode
from drb.nodes.logical_node import DrbLogicalNode
from drb.topics.resolver import resolve_children


class MockResourceNode(AbstractNode):
    def __init__(self, node: DrbNode):
        super().__init__()
        self._node = node

    @property
    def name(self) -> str:
        return self._node.name

    @property
    def namespace_uri(self) -> Optional[str]:
        return None

    @property
    def value(self) -> Optional[Any]:
        return None

    @property
    def attributes(self) -> Dict[Tuple[str, str], Any]:
        return {}

    @property
    def parent(self) -> Optional[DrbNode]:
        return self._node.parent

    @property
    def path(self) -> ParsedPath:
        return self._node.path / self.name

    @property
    @resolve_children
    def children(self) -> List[DrbNode]:
        child = DrbLogicalNode('sub', parent=self)
        sub_child = DrbLogicalNode('path', parent=child)
        sub_sub_child = DrbLogicalNode('foobar.data', parent=sub_child)
        child.append_child(sub_child)
        sub_child.append_child(sub_sub_child)
        return [child]

    def get_attribute(self, name: str, namespace_uri: str = None) -> Any:
        raise DrbException

    def has_child(self, name: str = None, namespace: str = None) -> bool:
        return True

    def close(self) -> None:
        pass

    def has_impl(self, impl: type, identifier: str = None) -> bool:
        return False

    def get_impl(self, impl: type, **kwargs) -> Any:
        raise DrbException


class MockResourceFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return MockResourceNode(node)
