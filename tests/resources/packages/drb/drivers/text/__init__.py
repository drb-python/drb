from drb.core import DrbNode, DrbFactory
from drb.exceptions.core import DrbFactoryException
from drb.nodes.logical_node import WrappedNode


class TextNode(WrappedNode):
    def __init__(self, node: DrbNode):
        super().__init__(node)


class TextFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return TextNode(node)
