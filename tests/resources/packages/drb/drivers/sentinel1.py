from drb.core import DrbNode, DrbFactory
from drb.nodes.logical_node import WrappedNode


class Sentinel1L0RFProduct(WrappedNode):
    def __init__(self, node: DrbNode):
        WrappedNode.__init__(self, node)


class Sentinel1L0RFProductFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return Sentinel1L0RFProduct(node)
