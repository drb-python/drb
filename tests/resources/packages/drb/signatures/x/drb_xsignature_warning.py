from drb.core import DrbNode
from drb.core.signature import Signature


class XSignatureDuplicate(Signature):
    """
    Allowing to check if a DRB Node match a specific XQuery.
    """
    def __init__(self, query: str):
        self._query_str = query

    def matches(self, node: DrbNode) -> bool:
        return 'OK' in node.name

    def to_dict(self) -> dict:
        return {self.get_name(): self._query_str}

    @staticmethod
    def get_name():
        return 'xsignature'


class XSignatureNoName(Signature):
    """
    Allowing to check if a DRB Node match a specific XQuery.
    """
    def __init__(self, query: str):
        self._query_str = query

    def matches(self, node: DrbNode) -> bool:
        return 'OK' in node.name

    def to_dict(self) -> dict:
        return {self.get_name(): self._query_str}

    @staticmethod
    def get_name():
        raise NotImplementedError
