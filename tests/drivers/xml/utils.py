from io import BytesIO, BufferedIOBase
from drb.core.node import DrbNode
from drb.core.factory import DrbFactory
from drb.nodes.logical_node import DrbLogicalNode


class MemoryNode(DrbLogicalNode):
    def __init__(self, name: str, content: bytes):
        super(MemoryNode, self).__init__(name)
        self.content = content
        self.add_impl(BufferedIOBase, self.__to_byte_stream, None)

    @staticmethod
    def __to_byte_stream(node, **kwargs) -> BytesIO:
        return BytesIO(node.content)


class DummyFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return node
