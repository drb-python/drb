from drb.addons.addon import Addon
from drb.core import DrbNode
from drb.topics.topic import DrbTopic


class GeoLocAddon(Addon):
    @classmethod
    def identifier(cls) -> str:
        return 'geolocation'

    @classmethod
    def return_type(cls) -> type:
        return dict

    def apply(self, node: DrbNode, **kwargs):
        return {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [0, 0]
            },
            "properties": {
                "key0": "value0",
                "key1": "value1"
            }
        }

    def can_apply(self, topic: DrbTopic):
        return False
