from drb.addons.addon import Addon
from drb.core import DrbNode
from drb.topics.topic import DrbTopic


class HelloAddon(Addon):
    @classmethod
    def identifier(cls) -> str:
        return 'hello'

    @classmethod
    def return_type(cls) -> type:
        return str

    def apply(self, node: DrbNode, **kwargs):
        return f'Hello {node.name}'

    def can_apply(self, source: DrbTopic):
        return True
