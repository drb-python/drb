(:*******************************************************:)
(: Description: Test lack of XQST0093 by importing a module without a circular dependency :)
(:********************************************************** :)

(: insert-start :)
import module namespace errata8_3a="http://www.w3.org/TestModules/errata8_3a" at "tests/xquery/files/import/errata8-module3a.xq";
declare variable $input-context external;
(: insert-end :)

errata8_3a:fun()
